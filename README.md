#  Felicia Gutierrez

#  Jonathan Teel

#  Elan Levy

#  CST 250

#  Project 3


#  Project3.txt

# main source file

.org 0x10000000

li $s0, 0xf0000000 # address of uart command register
li $s1, 0xf0000004 # address of the status register
li $s2, 0xf0000008 # address of the receive buffer
li $s3, 0xf000000c # address of the send buffer

li $a0, 0 # this will be used to store the # input prior to the operand.  Initialize to 0
li $a1, 0 # this will be used to store the current input being evaluated.  Initialize to 0
li $a2, 1 # used to toggle if a negative number is used.  initialize to 1
li $a3, 0 # this will be a counter for the # of digits in the input.  Initialize to 0

li $t3, 0x2d # hex value for "-".  Will be used to check for negative number.

li $s4, 0 # used to store the given operand in the equation.  initialize to zero

li $t7, 0 # COUNTER FOR DEBUGGING THE RECEIVE_INPUT LOOP

main:
polling_loop:

	ready_bit:		# poll for the ready bit
	# bitmask
	lw $t1, 4($s0)
	li $t0, 0b10
	and $t2, $t0, $t1
	beq $t0, $t2, receive_input # if ready bit is set, send the input to the command register
	nop

j polling_loop
nop
	receive_input:
	addiu $t7, $t7, 1 #increment debug counter

	bne $s6, $zero, reinitialize # if $s6 has been incremented, reinitialize all registers for a new equation
	nop
	continue-analysis:
	
	lw $t6, 0($s2)	# load recevied character into $t6
	
	#beq $t6, $t3, negative-number-toggle	# check if negative
	#nop
	li $t4, 0x30 #0
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x31 #2
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x32 #2
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x33 #3
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x34 #4
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x35 #5
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x36 #6
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x37 #7
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x38 #8
	beq $t6, $t4, add-numbers
	nop
	li $t4, 0x39 #9
	beq $t6, $t4, add-numbers
	nop

	li $a3, 0 #-------RESET THE DIGIT COUNTER TO ZERO

	li $t4, 0x3d # hex value for "=".  Will be used to check for end of input.
	beq $t6, $t4, compute-operand # check if we've reached the end of the input "=", if so 
	nop

	li $t4, 0x2b # hex value for "+".
	beq $t6, $t4, set-add-operand # add numbers
	nop

	li $t4, 0x2d # hex value for "-".
	beq $t6, $t4, set-sub-operand # subtract numbers
	nop

	li $t4, 0x2a # hex value for "*".
	beq $t6, $t4, set-mult-operand # multiply numbers
	nop


	invalid-content:
		# if the process gets this far, we have invalid content

j main
nop

	negative-number-toggle:  # process negative number
		li $a2, 0 #change to zero if a negative number is input
		j clear-status
		nop

	add-numbers:
		li $t9, 48 # load 48 into $t9 for subtraction calculation
		subu $t6, $t6, $t9 # subtract 48 to make the value of decimal correct.
		bne $a3, $zero, mult-ten # if the digit counter is not zero, multiply $a1 by ten
		nop
		continue-add:
		addiu $a3, $a3, 1 # increment the digit counter
		addu $a1, $a1, $t6 # complete addition of digits into $a1
		j clear-status
		nop
		

	complete:
		# convert negative if necessary
		beq $a2, $zero, neg-conversion # convert to neg number
		nop
		continue-complete:
		# place final value in $v0
		addiu $v0, $a0, 0 # load final value into $v0
		# create and send output
		li $s5, 0b1
		li $t9, 0x30
		sw $zero, 0($s3) # set send output to 0
		lw $t9, 0($s3) # View Command register before send attempt
		sw $s5, 0($s3) # Output to UART
		li $t9, 48 # set t9 to 48  (value of 0)
		lw $t9, 0($s0) # View Command Register after send attempt

		addiu $s6, $s6, 1 # increment the initialization counter

	clear-status:	# send clear status bit to command register
		sw $t0, 0($s0) # set clear status bit to 1

j main
nop

mult-ten:
	li $t9, 10
	mullo $a1, $a1, $t9
	j continue-add
	nop

neg-conversion:
	nor $a1, $a1, $a1 # invert a1
	addiu $a1, $a1, 1 # add 1
	j continue-complete
	nop

set-add-operand:
	bne $s4, $zero, compute-operand # if an operand is already stored, then complete the first part of the equation
	nop
	li $t8, 0
	addu $t8, $t8, $a1  #--------AFTER SCROLLING THROUGH ALL NUMBER INPUTS, STORE THE VALUE OF $a1 IN $t8
	continue-set-add-operand:
	li $s4, 0x2b # save the add operand into $s4
	li $a1, 0 #-------RESET $a1 TO ZERO
	j clear-status
	nop

set-sub-operand:
	bne $s4, $zero, compute-operand # if an operand is already stored, then complete the first part of the equation
	nop
	addiu $t8, $a1, 0  #--------AFTER SCROLLING THROUGH ALL NUMBER INPUTS, STORE THE VALUE OF $a1 IN $t8
	continue-set-sub-operand:
	li $s4, 0x2d # save the subtract operand into $s4
	li $a1, 0 #--------RESET $a1 TO ZERO
	j clear-status
	nop

set-mult-operand:
	bne $s4, $zero, compute-operand # if an operand is already stored, then complete the first part of the equation
	nop
	addiu $t8, $a1, 0  #--------AFTER SCROLLING THROUGH ALL NUMBER INPUTS, STORE THE VALUE OF $a1 IN $t8
	continue-set-mult-operand:
	li $s4, 0x2a # save the mult operand into $s4
	li $a1, 0 #--------RESET $a1 TO ZERO
	j clear-status
	nop


compute-operand:
	li $t4, 0x2b # hex value for "+".
	beq $s4, $t4, addition # add numbers
	nop

	li $t4, 0x2d # hex value for "-".
	beq $s4, $t4, subtraction # subtract numbers
	nop

	li $t4, 0x2a # hex value for "*".
	beq $s4, $t4, multiplication # multiply numbers
	nop

#----------------------METHOD FOR ADDING TWO INCOMING NUMBERS
addition:
	addu $a0, $a0, $t8 # add first number into $a0
	li $t8, 0 # once t8 is added into a0, set to zero
	addu $a0, $a0, $a1 # add the number before the operand $a0 and the number after the operand, $a1, into $a1
	li $t9, 0x3d # hex value for "="
	beq $t6, $t9, complete # if we reached the end of the input, complete the program and send output
	nop
	j continue-set-add-operand # if we haven't reached the end, continue the operation.
	nop

#----------------------METHOD FOR SUBTRACTING TWO INCOMING NUMBERS
subtraction:
	addu $a0, $a0, $t8 # add first number into $a0
	li $t8, 0 # once t8 is added into a0, set to zero
	subu $a0, $a0, $a1 # add the number before the operand $a0 and the number after the operand, $a1, into $a1
	li $t9, 0x3d # hex value for "="
	beq $t6, $t9, complete # if we reached the end of the input, complete the program and send output
	nop
	j continue-set-sub-operand
	nop

#----------------------METHOD FOR MULTIPLYING TWO INCOMING NUMBERS
multiplication:
	addu $a0, $a0, $t8 # add first number into $a0
	li $t8, 0 # once t8 is added into a0, set to zero
	mullo $a0, $a0, $a1 # multiply the lower bits and store in $a1
	#mulhi $t9, $a0, $a1 # store the higher bits in $t9
	li $t9, 0x3d # hex value for "="
	beq $t6, $t9, complete # if we reached the end of the input, complete the program and send output
	nop
	#bne $t9, $zero, overflow # overflow
	#nop
	j continue-set-mult-operand
	nop

overflow:
	# add code to send an error message for overflow
	j complete
	nop

reinitialize:
li $a0, 0 # this will be used to store the # input prior to the operand.  Initialize to 0
li $a1, 0 # this will be used to store the current input being evaluated.  Initialize to 0
li $a2, 1 # used to toggle if a negative number is used.  initialize to 1
li $a3, 0 # this will be a counter for the # of digits in the input.  Initialize to 0
li $s4, 0 # used to store the given operand in the equation.  initialize to zero
li $s6, 0
li $v0, 0 

j continue-analysis
nop












